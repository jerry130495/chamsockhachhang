<?php

class Base extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function delete_items($table, $key, $items) {
        $sql = "DELETE FROM $table WHERE $key IN ( ";
        if (is_array($items)) {
            $i = 0;
            foreach ($items as $item) {
                if ($i == 0) {
                    $sql .= "'" . $item . "'";
                }
                else {
                    $sql .= ", '" . $item . "'";
                }
                $i++;
            }
            $sql .= " ) ";
            $this->db->simple_query($sql);
            return $this->db->affected_rows();
        }
        else {
            return 0;
        }
    }

    public function delete($table, $where) {
        $this->db->where($where);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }

    public function update($table, $data, $where) {
        $sql = $this->db->update_string($table, $data, $where);
        return $this->db->simple_query($sql);
    }

    public function insert($table, $data, $return_key_value = TRUE) {
        if (!is_array($data)) {
            return 0;
        }
        else {
            $sql = $this->db->insert_string($table, $data);
            $this->db->simple_query($sql);
            if ($return_key_value != TRUE) {
                if ($this->db->affected_rows() > 0) {
                    return TRUE;
                }
                else {
                    return FALSE;
                }
            }
            else {
                if ($this->db->affected_rows() > 0) {
                    return $this->db->insert_id(); // nếu mà giá trị trả về khác 0 thì là đã Insert thành công
                }
                else {
                    return 0;
                }
            }
        }
    }

    public function get_item($table, $select = "", $where = "", $result_array = FALSE) {
        if ($select !== "") {
            $this->db->select($select);
        }
        if ($where !== "" OR is_array($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            if ($result_array == TRUE) {
                return $query->row_array();
            } else {
                return $query->row();    //return values not array
            }
        } else {
            return NULL;
        }
    }

    public function get_all($sql) {
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_items($table, $select = "", $where = "", $orderby = "", $start = 0, $limit = 30) {
        if ($select !== "") {
            $this->db->select($select);
        }
        // $this->db->from($table);
        if ($where !== "" OR is_array($where)) {
            $this->db->where($where);
        }
        if ($orderby !== "") {
            $this->db->order_by($orderby);
        }
        // $query = $this->db->get($table);
        $this->db->limit($limit, $start);

        $sql = $this->db->get_compiled_select($table); //return SQL query as a string
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function C_get_items($table, $select = "", $where = "", $like = "", $orderby = "", $start = 0, $limit = 30,$tableJoin = "",$condition="", $type="") {
        if ($select !== "") {
            $this->db->select($select);
        }
        // $this->db->from($table);
        if ($where !== "" OR is_array($where)) {
            $this->db->where($where);
        }

        if($like !== "" OR is_array(($like))){
            $this->db->like($like);
        }

        if ($orderby !== "") {
            $this->db->order_by($orderby);
        }
        // $query = $this->db->get($table);
        $this->db->limit($limit, $start);

        $sql = $this->db->get_compiled_select($table); //return SQL query as a string
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function getHoTro($like = "",$where = ""){
        $this->db->select('banhotro.MaHoTro,khachhang.TenKH,banhotro.NoiDungYeuCau, banhotro.NgayYeuCau, banphanhoi.NgayPH, banphanhoi.NoiDungPH');
        $this->db->from('banhotro');
        if($where !== ""){
            $this->db->where($where);
        }
        if($like !== ""){
            $this->db->like($like);
        }
        $this->db->join('khachhang','khachhang.MaKH = banhotro.MaKH');
        $this->db->join('banphanhoi', 'banhotro.MaHoTro = banphanhoi.MaHoTro', 'left');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }
}