<div id="main">
    <div id="content">
        <div id="left">
            <h3>Giới thiệu</h3>
            <p>
                Tên doanh nghiệp : Công ty trách nhiệm hữu hạn đầu tư sản xuất Việt Trung.
                Mã số thuế: 0101728042
                Địa chỉ: Cụm công nghiệp thị trấn Phùng, Thị trấn Phùng, Huyện Đan Phượng, Thành phố Hà Nội.
                Ngày thành lập: 10/08/2005
                Điện thoại: 0989121085
                Fax: 38583825
                Công ty trách nhiệm hữu hạn đầu tư sản xuất Việt Trung hoạt động kinh doanh chính trong lĩnh vực như: gia công và bán sản phẩm máy móc và thiết bị cơ khí, Tư vấn và lập dự án đầu tư sản phẩm, xây dựng công trình kỹ thuật dân dụng, …
                Qua nhiều năm hình thành và phát triển công ty luôn nghiên cứu những kỹ thuật tiên tiến nhằm cải thiện chất lượng sản phẩm ngày càng tốt hơn.
                Mục tiêu của công ty chúng tôi: Cung cấp các sản phẩm và dịch vụ chất lượng cao đến người tiêu dùng.
                Nguyên tắc hoạt động: Uy tín và chất lượng
                Với hàng hóa đảm bảo chất lượng tốt. Đội ngũ nhân viên chuyên nghiệp, với nhiều năm kinh nghiệm trong lĩnh vực sản xuất vật liệu xây dựng sẵn sàng phục vụ khách hàng nhanh nhất và tốt nhất. Bộ phận hỗ trợ sau bán hàng luôn nhiệt tình giải đáp các thắc mắc cho khách hàng. Chúng tôi có những đãi ngộ với những dịch vụ tốt nhất. Chúng tôi  hy vọng sẽ trở thành người bạn đồng hành không thể thiếu của mỗi gia đình.
                Clever: Sự lựa chọn thông minh!
            </p></div>
        <div id="right">
            <img src="<?php echo base_url().'public/img/'; ?>test.PNG" alt="Hệ thống chăm sóc khách hàng" />
        </div>
    </div>
</div>