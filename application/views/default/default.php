<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Chăm sóc khách hàng</title>
    <link type="text/css" href="<?php echo base_url(); ?>style.css" rel="stylesheet" />
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="logo">
            <div id="symbol">
                <img src="<?php echo base_url().'public/img/'; ?>test.PNG" alt="Hệ thống chăm sóc khách hàng" />
            </div>
            <div id="title">
                <a href="<?php echo base_url(); ?>">
                    <h1>hệ thống quản lý chăm sóc khách hàng</h1>
                    <p>công ty Trách nhiệm hữu hạn đầu tư sản xuất Việt Trung</p>
                </a>
            </div>
        </div>
        <div id="button">
            <?php
                if(isset($this->session->userdata['username'])){
                    echo
                    "<a href=\"\">
                        Xin chào, ".$this->session->userdata['username']."
                    </a>
                    <a href=\"".base_url()."index.php/C_DangNhap/logout"."\">
                        Thoát
                    </a>";
                }
                else{
                    echo "<a href=\"".base_url()."index.php/C_DangNhap/login"."\">
                        Đăng nhập
                    </a>";
                }
            ?>
        </div>
        <div id="menu">
            <ul>
                <li>
                    <a href="<?php echo base_url(); ?>">Trang chủ</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>index.php/C_KhachHang">Khách hàng</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>index.php/C_ChuongTrinh">Chương trình</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>index.php/C_HoTro">Hỗ trợ</a>
                </li>
                <li>
                    <a href="#">Báo cáo</a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/C_DanhSachKhachHangMoi">Báo cáo DSKH mới</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/C_KhachHang/themchuongtrinhapdung">Báo cáo thông tin CSKH</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="main">
        <?php $this->load->view($subview); ?>
    </div>
    <div id="footer">
        <p>Bản quyền thuộc về <a href="#">Jerry</a></p>
    </div>
</div>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/datepicker/jquery-ui.min.css">
<script src="<?php echo base_url(); ?>public/datepicker/jquery.js"></script>
<script src="<?php echo base_url(); ?>public/datepicker/jquery-ui.min.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker();
    } );
</script>

<script src="<?php echo base_url(); ?>public/ckeditor/ckeditor.js" language="javascript"></script>
<script>
    CKEDITOR.replace('editor');
</script>
</body>
</html>



























