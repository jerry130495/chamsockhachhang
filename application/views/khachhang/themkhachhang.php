<?php
/**
 * Created by PhpStorm.
 * User: Tung
 * Date: 3/28/2017
 * Time: 11:31 AM
 */
?>
    <?php  if($this->session->flashdata("MessKhachHang") != '') echo "<i style='color:red'>".$this->session->flashdata("MessKhachHang")."</i>" ?>
    <?php if(isset($khachhang)) echo "<form action=\"".base_url()."index.php/C_KhachHang/update\" method=\"post\">";
            else echo "<form action=\"".base_url()."index.php/C_KhachHang/submitInsert\" method=\"post\">";
    ?>
        <table id="themkhachhang">
            <tr>
                <td>
                    <p>Mã khách hàng</p>
                </td>
                <td>
                    <input pattern="K+H+[0-9]{1,}" onchange="try{setCustomValidity('')}catch(e){}"
                           oninvalid="setCustomValidity('Chữ cái đầu tiên là KH rồi đến số.\n Ví dụ: KH001')" type="text"
                           name="MaKH" value="<?php if(isset($khachhang)) echo $khachhang->MaKH; else echo ""; ?>" required>
                </td>
            </tr>
            <tr>
                <td>
                    Tên khách hàng
                </td>
                <td>
                    <input type="text" name="TenKH" value="<?php echo ((isset($khachhang))?$khachhang->TenKH:""); ?>"  required>
                </td>
            </tr>
            <tr>
                <td>
                    SĐT
                </td>
                <td>
                    <input pattern="[0-9]{1,}" type="text" name="SDT" value="<?php echo ((isset($khachhang))?$khachhang->SDT:""); ?>" >
                </td>
            </tr>
            <tr>
                <td>
                    Email
                </td>
                <td>
                    <input type="email" name="Email" value="<?php echo ((isset($khachhang))?$khachhang->Email:""); ?>" >
                </td>
            </tr>
            <tr>
                <td>
                    Ngành nghề
                </td>
                <td>
                    <input type="text" name="NganhNghe" value="<?php echo ((isset($khachhang))?$khachhang->NganhNghe:""); ?>" >
                </td>
            </tr>
            <tr>
                <td>
                    Chức vụ
                </td>
                <td>
                    <input type="text" name="ChucVu" value="<?php echo ((isset($khachhang))?$khachhang->ChucVu:""); ?>" >
                </td>
            </tr>
            <tr>
                <td>
                    Địa chỉ
                </td>
                <td>
                    <input type="text" name="DiaChi" value="<?php echo ((isset($khachhang))?$khachhang->DiaChi:""); ?>" >
                </td>
            </tr>
            <tr>
                <td>
                    Loại khách hàng
                </td>
                <td>
                    <select name="MaLoaiKH">
                        <option value="all">Tất cả</option>
                        <?php
                        $select_data = $this->session->userdata("objLoaiKH");
                        if(isset($select_data) ){
                            foreach ($select_data as $item){
                                if(isset($khachhang)){
                                    if($khachhang->MaLoaiKH == $item->MaLoaiKH){
                                        echo "<option value=\"".$item->MaLoaiKH."\" selected>
                                            ".$item->TenLoaiKH."
                                        </option>";
                                    }
                                }
                                else{
                                    echo "<option value=\"".$item->MaLoaiKH."\">
                                        ".$item->TenLoaiKH."
                                    </option>";
                                }
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" name="themkhachhang" value="Thêm" >
                    <a href="<?php echo base_url(); ?>index.php/C_KhachHang">Thoát</a>
                </td>
            </tr>
        </table>
    </form>

<?php
