<div id="top">
    <form action="<?php echo base_url() ?>index.php/C_KhachHang/search" method="post">
        <div class="left">
            <p>Danh sách khách hàng
                <select name="loaiKhachHang">
                    <option value="all">
                        Tất cả
                    </option>
                    <?php
                    $select_data = $this->session->userdata("objLoaiKH");
                    if(isset($select_data) ){
                        foreach ($select_data as $item){
                            echo "<option value=\"".$item->MaLoaiKH."\">
                                ".$item->TenLoaiKH."
                            </option>";
                        }
                    }
                    ?>
                </select>
            </p>
            <p>
                <input type="text" placeholder="Tên khách hàng ..." value="" name="tenKH" />
                <input type="submit" name="timKiem" value="Tìm kiếm" />
            </p>
        </div>
        <div class="right">
            <a href="<?php echo base_url(); ?>index.php/C_KhachHang/insert">Thêm mới</a>
        </div>
    </form>
    <?php  if($this->session->flashdata("MessKhachHang") != '') echo "<i style='color:red'>".$this->session->flashdata("MessKhachHang")."</i>" ?>
</div>
<div id="bottom">
    <table>
        <thead>
            <tr>
                <th>Mã KH</th>
                <th>Tên KH</th>
                <th>SĐT</th>
                <th>Email</th>
                <th>Ngành nghề</th>
                <th>Chức vụ</th>
                <th>Địa chỉ</th>
                <th>Thêm CTAD</th>
                <th>Sửa</th>
                <th>Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($khachhang)){
                    foreach ($khachhang as $item) {
                        echo "<tr>
                            <td>".$item->MaKH."</td>
                            <td>".$item->TenKH."</td>
                            <td>".$item->SDT."</td>
                            <td>".$item->Email."</td>
                            <td>".$item->NganhNghe."</td>
                            <td>".$item->ChucVu."</td>
                            <td>".$item->DiaChi."</td>
                            <td><a href=\"".base_url()."index.php/C_KhachHang/themchitietchuongtrinh?id=".$item->MaKH."\"><img width=\"20px\" src=\"".base_url()."public/img/add.png\" ></a></td>
                            <td><a href=\"".base_url()."index.php/C_KhachHang/insert?id=".$item->MaKH."\"><img width=\"20px\" src=\"".base_url()."public/img/edit.png\"  ></a></td>
                            <td><a href=\"".base_url()."index.php/C_KhachHang/delete?id=".$item->MaKH."\"><img width=\"20px\" src=\"".base_url()."public/img/delete.png\"  ></a></td>
                        </tr>";
                    }
                }
            ?>
        </tbody>
    </table>
</div>