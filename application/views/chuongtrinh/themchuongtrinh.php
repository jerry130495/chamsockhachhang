<?php
/**
 * Created by PhpStorm.
 * User: Tai Linhing
 * Date: 3/28/2017
 * Time: 11:31 AM
 */
?>
<?php  if($this->session->flashdata("MessChuongTrinh") != '') echo "<i style='color:red'>".$this->session->flashdata("MessChuongTrinh")."</i>" ?>
    <?php
        if(isset($chuongtrinh)) echo "<form action=\"".base_url()."index.php/C_ChuongTrinh/update\" method=\"post\">";
        else echo "<form action=\"".base_url()."index.php/C_ChuongTrinh/submitInsert\" method=\"post\">";
    ?>
        <table id="themkhachhang">
            <tr>
                <td>
                    <p>Mã chương trình</p>
                </td>
                <td>
                    <input pattern="C+T+[0-9]{1,}" onchange="try{setCustomValidity('')}catch(e){}"
                           oninvalid="setCustomValidity('Chữ cái đầu tiên là CT rồi đến số.\n Ví dụ: CT001')" type="text"
                           name="MaCT" value="<?php if(isset($chuongtrinh)) echo $chuongtrinh->MaCT; else echo ""; ?>" required>
                </td>
            </tr>
            <tr>
                <td>
                    Tên chương trình
                </td>
                <td>
                    <input type="text" name="TenCT" value="<?php echo ((isset($chuongtrinh))?$chuongtrinh->TenCT:""); ?>"  required>
                </td>
            </tr>
            <tr>
                <td>
                    Loại khách hàng
                </td>
                <td>
                    <select name="MaLoaiKH" required>
                        <option value="all">Tất cả</option>
                        <?php
                        $select_data = $this->session->userdata("objLoaiKH");
                        if(isset($select_data) ){
                            foreach ($select_data as $item){
                                if(isset($chuongtrinh)){
                                    if($chuongtrinh->MaLoaiKH == $item->MaLoaiKH){
                                        echo "<option value=\"".$item->MaLoaiKH."\" selected>
                                            ".$item->TenLoaiKH."
                                        </option>";
                                    }
                                }
                                else{
                                    echo "<option value=\"".$item->MaLoaiKH."\">
                                        ".$item->TenLoaiKH."
                                    </option>";
                                }
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Nội dung
                </td>
                <td>
                    <textarea id="editor" rows="4" name="MoTa" required><?php echo ((isset($chuongtrinh))?$chuongtrinh->MoTa:""); ?></textarea>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" name="themchuongtrinh" value="Thêm" >
                    <a href="<?php echo base_url(); ?>index.php/C_ChuongTrinh">Thoát</a>
                </td>
            </tr>
        </table>
    </form>

<?php
