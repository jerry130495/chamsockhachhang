<div id="top">
    <form action="<?php echo base_url() ?>index.php/C_ChuongTrinh/search" method="post">
        <div class="left">
            <p>Danh sách chương trình
                <select name="loaiKhachHang">
                    <option value="all">
                        Tất cả
                    </option>
                    <?php
                    $select_data = $this->session->userdata("objLoaiKH");
                    if(isset($select_data) ){
                        foreach ($select_data as $item){
                            echo "<option value=\"".$item->MaLoaiKH."\">
                                ".$item->TenLoaiKH."
                            </option>";
                        }
                    }
                    ?>
                </select>
            </p>
            <p>
                <input type="text" placeholder="Tên chương trình ..." value="" name="tenCT" />
                <input type="submit" name="timKiem" value="Tìm kiếm" />
            </p>
        </div>
        <div class="right">
            <a href="<?php echo base_url(); ?>index.php/C_ChuongTrinh/insert">Thêm mới</a>
        </div>
    </form>
    <?php  if($this->session->flashdata("MessKhachHang") != '') echo "<i style='color:red'>".$this->session->flashdata("MessChuongTrinh")."</i>" ?>
</div>
<div id="bottom">
    <table>
        <thead>
        <tr>
            <th>Mã CT</th>
            <th>Tên CT</th>
            <th>Đối tượng áp dụng</th>
            <th>Nội dung</th>
            <th>Xem chi tiết</th>
            <th>Sửa</th>
            <th>Xóa</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(isset($chuongtrinh)){
            foreach ($chuongtrinh as $item) {
                echo "<tr>
                            <td>".$item->MaCT."</td>
                            <td>".$item->TenCT."</td>
                            <td>".$item->TenLoaiKH."</td>
                            <td>".substr($item->MoTa,0,20)." ....</td>
                            <td><a href=\"".base_url()."index.php/C_ChuongTrinh/view?id=".$item->MaCT."\"><img width=\"20px\" src=\"".base_url()."public/img/find.png\" ></a></td>
                            <td><a href=\"".base_url()."index.php/C_ChuongTrinh/insert?id=".$item->MaCT."\"><img width=\"20px\" src=\"".base_url()."public/img/edit.png\"  ></a></td>
                            <td><a href=\"".base_url()."index.php/C_ChuongTrinh/delete?id=".$item->MaCT."\"><img width=\"20px\" src=\"".base_url()."public/img/delete.png\"  ></a></td>
                        </tr>";
            }
        }
        ?>
        </tbody>
    </table>
</div><?php
/**
 * Created by PhpStorm.
 * User: Tung
 * Date: 3/28/2017
 * Time: 3:43 PM
 */