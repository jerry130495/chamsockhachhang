<?php
/**
 * Created by PhpStorm.
 * User: Tung
 * Date: 3/28/2017
 * Time: 11:31 AM
 */
?>

    <form action="<?php echo base_url(); ?>index.php/C_DangNhap/submitLogin" method="post">
        <table id="login">
            <tr>
                <td>
                    <p>Tên đăng nhập</p>
                </td>
                <td>
                    <input type="text" name="username" value="" >
                </td>
            </tr>
            <tr>
                <td>
                    Mật khẩu
                </td>
                <td>
                    <input type="password" name="password" value="" >
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" name="login" value="Đăng nhập" >
                    <input type="reset" value="Làm mới" >
                </td>
            </tr>
            <?php
                if($this->session->flashdata("errLogin") != '')
                echo "
                    <tr>
                        <td colspan='2'>
                            <i style='color:red'>".$this->session->flashdata('errLogin')."</i>
                        </td>
                    </tr>";
            ?>
        </table>
    </form>

<?php
