<?php
/**
 * Created by PhpStorm.
 * User: Tai Tling
 * Date: 3/28/2017
 * Time: 11:31 AM
 */
?>
<?php  if($this->session->flashdata("Messhotro") != '') echo "<i style='color:red'>".$this->session->flashdata("Messhotro")."</i>" ?>
    <?php
        if(isset($hotro)) echo "<form action=\"".base_url()."index.php/C_HoTro/update\" method=\"post\">";
        else echo "<form action=\"".base_url()."index.php/C_HoTro/submitInsert\" method=\"post\">";
    ?>
        <table id="themkhachhang">
            <tr>
                <td>
                    <p>Mã Hỗ trợ</p>
                </td>
                <td>
                    <input pattern="H+T+[0-9]{1,}" onchange="try{setCustomValidity('')}catch(e){}"
                           oninvalid="setCustomValidity('Chữ cái đầu tiên là HT rồi đến số.\n Ví dụ: HT001')" type="text"
                           name="MaHT" value="<?php if(isset($hotro)) echo $hotro->MaHoTro; else echo ""; ?>" required>
                </td>
            </tr>
            <tr>
                <td>
                    Mã khách hàng
                </td>
                <td>
                    <input type="text" name="maKH" value="<?php echo ((isset($hotro))?$hotro->MaKH:""); ?>"  required>
                </td>
            </tr>
            <tr>
                <td>
                    Nội dung yêu cầu
                </td>
                <td>
                    <textarea id="editor" rows="4" name="NoiDungYeuCau" required><?php echo ((isset($hotro))?$hotro->NoiDungYeuCau:""); ?></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    Ngày yêu cầu
                </td>
                <td>
                    <input type="text" id="datepicker" name="NgayYeuCau" value="<?php echo ((isset($hotro))?$hotro->NgayYeuCau:""); ?>"  required>
                </td>
            </tr>
            <tr>
                <td>
                    Mã dịch vụ
                </td>
                <td>
                    <select name="MaDichVu" required>
                        <?php
                        $select_data = $this->session->userdata("objDichVu");
                        if(isset($select_data) ){
                            foreach ($select_data as $item){
                                if(isset($hotro)){
                                    if($hotro->MaDichVu == $item->MaDichVu){
                                        echo "<option value=\"".$item->MaDichVu."\" selected>
                                            ".$item->TenDichVu."
                                        </option>";
                                    }
                                }
                                else{
                                    echo "<option value=\"".$item->MaDichVu."\">
                                            ".$item->TenDichVu."
                                        </option>";
                                }

                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" name="themhotro" value="Cập nhật" >
                    <a href="<?php echo base_url(); ?>index.php/C_HoTro">Thoát</a>
                </td>
            </tr>
        </table>
    </form>

<?php
