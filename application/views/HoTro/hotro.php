<div id="top">
    <form action="<?php echo base_url() ?>index.php/C_HoTro/search" method="post">
        <div class="left">
            <p>Danh sách các yêu cầu hỗ trợ
                <select name="loaiKhachHang">
                    <option value="all">
                        Tất cả
                    </option>
                    <?php
                    $select_data = $this->session->userdata("objLoaiKH");
                    if(isset($select_data) ){
                        foreach ($select_data as $item){
                            echo "<option value=\"".$item->MaLoaiKH."\">
                                ".$item->TenLoaiKH."
                            </option>";
                        }
                    }
                    ?>
                </select>
            </p>
            <p>
                <input type="text" placeholder="Tên hỗ trợ ..." value="" name="maHT" />
                <input type="submit" name="timKiem" value="Tìm kiếm" />
            </p>
        </div>
        <div class="right">
            <a href="<?php echo base_url(); ?>index.php/C_HoTro/insert">Thêm mới</a>
        </div>
    </form>
    <?php  if($this->session->flashdata("MessHoTro") != '') echo "<i style='color:red'>".$this->session->flashdata("MessHoTro")."</i>" ?>
</div>
<div id="bottom">
    <table>
        <thead>
        <tr>
            <th>Mã hỗ trợ</th>
            <th>Khách hàng</th>
            <th>Nội dung</th>
            <th>Ngày gửi</th>
            <th>Ngày xử lý</th>
            <th>Nội dung xử lý</th>
            <th>Xem chi tiêt</th>
            <th>Sửa</th>
            <th>Xóa</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(isset($hotro)){
            foreach ($hotro as $item) {
                echo "<tr>
                            <td>".$item->MaHoTro."</td>
                            <td>".$item->TenKH."</td>
                            <td>".$item->NoiDungYeuCau."</td>
                            <td>".$item->NgayYeuCau."</td>
                            <td>".$item->NgayPH."</td>
                            <td>".$item->NoiDungPH."</td>
                            <td><a href=\"".base_url()."index.php/C_HoTro/view?id=".$item->MaHoTro."\"><img width=\"20px\" src=\"".base_url()."public/img/find.png\" ></a></td>
                            <td><a href=\"".base_url()."index.php/C_HoTro/insert?id=".$item->MaHoTro."\"><img width=\"20px\" src=\"".base_url()."public/img/edit.png\"  ></a></td>
                            <td><a href=\"".base_url()."index.php/C_HoTro/delete?id=".$item->MaHoTro."\"><img width=\"20px\" src=\"".base_url()."public/img/delete.png\"  ></a></td>
                        </tr>";
            }
        }
        ?>
        </tbody>
    </table>
</div><?php
/**
 * Created by PhpStorm.
 * User: Tung
 * Date: 3/28/2017
 * Time: 3:43 PM
 */