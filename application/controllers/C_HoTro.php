<?php
/**
 * Created by PhpStorm.
 * User: Tai Tling
 * Date: 3/28/2017
 * Time: 11:29 AM
 */

class C_HoTro extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("session","form_validation"));
        $this->load->helper("url");
        $this->load->model("Base");
    }

    protected $data;

    public function index(){
        $username = $this->session->userdata('username');
        if(!isset($username)){
            header("location: ".base_url());
        }
        //print_r($this->session->userdata('objDichVu'));
        $data['hotro'] = $this->Base->getHoTro();
        //print_r($data['select_data']);
        $data['subview'] = 'hotro/hotro';
        $this->load->view("default/default",$data);
    }

    public function search(){
        $loaiKH = $this->input->post("loaiKhachHang");
        $maHT = $this->input->post("maHT");

        $like = array(
            "BanHoTro.MaHoTro" => $maHT
        );

        $data['hotro'] = $this->Base->getHoTro($like);
        //echo $data['hotro'];
        $data['subview'] = 'hotro/hotro';
        $this->load->view("default/default",$data);
    }

    public function view(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaHoTro" =>  $this->input->get("id")
            );
            $data['hotro'] = $this->Base->get_item("banhotro","NoiDungYeuCau", $where, FALSE );
            $data['subview'] = 'hotro/xemchitiet';
            $this->load->view("default/default",$data);
        }
    }

    public function insert(){
        $this->session->set_userdata('objDichVu',$this->Base->get_items("dichvu","*","","",0,1000));
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaHoTro" =>   $this->input->get("id")
            );
            $data['hotro'] = $this->Base->get_item("banhotro","*",$where);
        }
        $data['subview'] = 'hotro/themhotro';
        $this->load->view("default/default",$data);
    }

    public function submitInsert(){
        if($this->input->post("themhotro") != null){
            $dataInsert = array(
                "MaHoTro" => $this->input->post("MaHT"),
                "MaKH" => $this->input->post("maKH"),
                "NoiDungYeuCau" => $this->input->post("NoiDungYeuCau"),
                "NgayYeuCau" => $this->input->post("NgayYeuCau"),
                "MaDichVu" => $this->input->post("MaDichVu")
            );

            if($this->Base->insert("banhotro", $dataInsert, false) != FALSE){
                $this->session->set_flashdata('Messhotro','Cập nhật thành công');
                $data['subview'] = 'hotro/themhotro';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('Messhotro','Cập nhật thất bại');
                $data['subview'] = 'hotro/themhotro';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'hotro/hotro';
            $this->load->view("default/default",$data);
        }
    }

    public function update(){
        //echo $this->Base->update("banhotro", $dataUpdate, array("MaHoTro" => $this->input->post("MaHT"))  );
        if($this->input->post("themhotro") != null){
            $dataUpdate = array(
                "MaKH" => $this->input->post("maKH"),
                "NoiDungYeuCau" => $this->input->post("NoiDungYeuCau"),
                "NgayYeuCau" => $this->input->post("NgayYeuCau"),
                "MaDichVu" => $this->input->post("MaDichVu")
            );

            if($this->Base->update("banhotro", $dataUpdate, array("MaHoTro" => $this->input->post("MaHT"))  )){
                $this->session->set_flashdata('Messhotro','Cập nhật thành công');
                $data['subview'] = 'hotro/themhotro';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('Messhotro','Cập nhật thất bại');
                $data['subview'] = 'hotro/themhotro';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'hotro/hotro';
            $this->load->view("default/default",$data);
        }
    }

    public function delete(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaCT" =>   $this->input->get("id")
            );

            if($this->Base->delete("hotro",$where)>0){
                $this->session->set_flashdata('Messhotro','Cập nhật thành công');
                $data['subview'] = 'hotro/hotro';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('Messhotro','Cập nhật thất bại');
                $data['subview'] = 'hotro/hotro';
                $this->load->view("default/default",$data);
            }
        }
    }
}