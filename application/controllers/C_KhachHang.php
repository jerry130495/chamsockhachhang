<?php

class C_KhachHang extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("session","form_validation"));
        $this->load->helper("url");
        $this->load->model("Base");
    }

    protected $data;

    public function index(){
        $username = $this->session->userdata('username');
        if(!isset($username)){
            header("location: ".base_url());
        }
        $this->session->set_userdata('objLoaiKH',$this->Base->get_items("LoaiKhachHang","*","","",0,1000));
        $data['khachhang'] = $this->Base->C_get_items("KhachHang","*", "", "", "", "", 0, 1000 );
        //print_r($data['select_data']);
        $data['subview'] = 'khachhang/khachhang';
        $this->load->view("default/default",$data);
    }

    public function search(){
        $loaiKH = $this->input->post("loaiKhachHang");
        $tenKH = $this->input->post("tenKH");

        if($loaiKH == 'all'){
            $where = "";
        }
        else{
            $where = array(
                "MaLoaiKH" => $loaiKH
            );
        }

        $like = array(
            "TenKH" => $tenKH
        );

        $data['khachhang'] = $this->Base->C_get_items("KhachHang","*", $where, $like, "", "", 0, 1000 );

        $data['subview'] = 'khachhang/khachhang';
        $this->load->view("default/default",$data);
    }

    public function insert(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaKH" =>   $this->input->get("id")
            );
            $data['khachhang'] = $this->Base->get_item("KhachHang","*",$where);
        }
        $data['subview'] = 'khachhang/themkhachhang';
        $this->load->view("default/default",$data);
    }

    public function submitInsert(){
        if($this->input->post("themkhachhang") != null){
            $dataInsert = array(
                "MaKH" => $this->input->post("MaKH"),
                "TenKH" => $this->input->post("TenKH"),
                "DiaChi" => $this->input->post("DiaChi"),
                "SDT" => $this->input->post("SDT"),
                "Email" => $this->input->post("Email"),
                "NganhNghe" => $this->input->post("NganhNghe"),
                "ChucVu" => $this->input->post("ChucVu"),
                "MaLoaiKH" => $this->input->post("MaLoaiKH")
            );

            if($this->Base->insert("KhachHang", $dataInsert, false) != FALSE){
                $this->session->set_flashdata('MessKhachHang','Cập nhật thành công');
                $data['subview'] = 'khachhang/themkhachhang';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('MessKhachHang','Cập nhật thất bại');
                $data['subview'] = 'khachhang/themkhachhang';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'khachhang/khachhang';
            $this->load->view("default/default",$data);
        }
    }

    public function update(){
        if($this->input->post("themkhachhang") != null){
            $dataInsert = array(
                "MaKH" => $this->input->post("MaKH"),
                "TenKH" => $this->input->post("TenKH"),
                "DiaChi" => $this->input->post("DiaChi"),
                "SDT" => $this->input->post("SDT"),
                "Email" => $this->input->post("Email"),
                "NganhNghe" => $this->input->post("NganhNghe"),
                "ChucVu" => $this->input->post("ChucVu"),
                "MaLoaiKH" => $this->input->post("MaLoaiKH")
            );

            if($this->Base->update("KhachHang", $dataInsert, array("MaKH" => $this->input->post("MaKH"))) != FALSE){
                $this->session->set_flashdata('MessKhachHang','Cập nhật thành công');
                $data['subview'] = 'khachhang/themkhachhang';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('MessKhachHang','Cập nhật thất bại');
                $data['subview'] = 'khachhang/themkhachhang';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'khachhang/khachhang';
            $this->load->view("default/default",$data);
        }
    }

    public function delete(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaKH" =>   $this->input->get("id")
            );

            if($this->Base->delete("KhachHang",$where)>0){
                $this->session->set_flashdata('MessKhachHang','Cập nhật thành công');
                $data['subview'] = 'khachhang/khachhang';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('MessKhachHang','Cập nhật thất bại');
                $data['subview'] = 'khachhang/khachhang';
                $this->load->view("default/default",$data);
            }
        }
    }

    public function themchuongtrinhapdung(){
        $username = $this->session->userdata('username');
        if(!isset($username)){
            header("location: ".base_url());
        }
        $data['MaKH'] = $this->input->get("id");
        $this->session->set_userdata('objLoaiKH',$this->Base->get_items("LoaiKhachHang","*","","",0,1000));
        $data['chuongtrinhapdung'] = $this->Base->C_get_items("chitietchuongtrinh,khachhang,nhanvien","*", "`chitietchuongtrinh`.`MaNV` = `nhanvien`.`MaNV` and `khachhang`.`MaKH` = `chitietchuongtrinh`.`MaKH`", "", "", "", 0, 1000 );
        //print_r($data['select_data']);
        $data['subview'] = 'khachhang/themchuongtrinhapdung';
        $this->load->view("default/default",$data);
    }

    public function searchMore(){
        $maCTAD = $this->input->post("maCTAD");

        $where = "`chitietchuongtrinh`.`MaNV` = `nhanvien`.`MaNV` and `khachhang`.`MaKH` = `chitietchuongtrinh`.`MaKH` and MaCTCT like '%".$maCTAD."%'";
        

        $data['chuongtrinhapdung'] = $this->Base->C_get_items("chitietchuongtrinh,khachhang,nhanvien","*", $where, "", "", 0, 1000 );

        $data['subview'] = 'khachhang/themchuongtrinhapdung';
        $this->load->view("default/default",$data);
    }

    public function themchitietchuongtrinh(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaCTCT" =>   $this->input->get("id")
            );
            $data['chitietchuongtrinh'] = $this->Base->get_item("chitietchuongtrinh","*",$where);
        }
        $this->session->set_userdata("chuongtrinh", $this->Base->get_items("ChuongTrinh","MaCT,TenCT", "", FALSE ));
        $this->session->set_userdata("khachhang", $this->Base->get_items("khachhang","MaKH, TenKH", "", TRUE ));
        $this->session->set_userdata("nhanvien", $this->Base->get_items("nhanvien","MaNV, HoTenNV", "", TRUE ));
        $data['subview'] = 'khachhang/themchitietchuongtrinh';
        $this->load->view("default/default",$data);
    }

    public function submitInsertCTCT(){
        if($this->input->post("themchitietchuongtrinh") != null){
            $dataInsert = array(
                "MaCTCT" => $this->input->post("MaCTCT"),
                "MaCT" => $this->input->post("MaCT"),
                "MaKH" => $this->input->post("MaKH"),
                "MaNV" => $this->input->post("MaNV"),
                "Diple" => $this->input->post("DipLe"),
                "Ngay" => $this->input->post("Ngay"),
                "HinhThuc" => $this->input->post("HinhThuc"),
                "GiaiTri" => $this->input->post("GiaiTri")
            );
            if($this->Base->insert("chitietchuongtrinh", $dataInsert, false) != FALSE){
                $this->session->set_flashdata('Messchitietchuongtrinh','Cập nhật thành công');
                $data['subview'] = 'khachhang/themchitietchuongtrinh';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('Messchitietchuongtrinh','Cập nhật thất bại');
                $data['subview'] = 'khachhang/themchitietchuongtrinh';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'khachhang/themchitietchuongtrinh';
            $this->load->view("default/default",$data);
        }
    }

    public function updateCTCT(){
        if($this->input->post("themchitietchuongtrinh") != null){
            $dataInsert = array(
                "MaCT" => $this->input->post("MaCT"),
                "MaKH" => $this->input->post("MaKH"),
                "MaNV" => $this->input->post("MaNV"),
                "Diple" => $this->input->post("DipLe"),
                "Ngay" => $this->input->post("Ngay"),
                "HinhThuc" => $this->input->post("HinhThuc"),
                "GiaiTri" => $this->input->post("GiaiTri")
            );
            if($this->Base->update("chitietchuongtrinh", $dataInsert, array( "MaCTCT" => $this->input->post("MaCTCT") )) != FALSE){
                $this->session->set_flashdata('Messchitietchuongtrinh','Cập nhật thành công');
                $data['subview'] = 'khachhang/themchitietchuongtrinh';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('Messchitietchuongtrinh','Cập nhật thất bại');
                $data['subview'] = 'khachhang/themchitietchuongtrinh';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'khachhang/themchitietchuongtrinh';
            $this->load->view("default/default",$data);
        }
    }

    public function deleteCTCT(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaCTCT" =>   $this->input->get("id")
            );

            if($this->Base->delete("chitietchuongtrinh",$where)>0){
                $this->session->set_flashdata('Messchitietchuongtrinh','Cập nhật thành công');
                $data['subview'] = 'khachhang/themchuongtrinhapdung';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('Messchitietchuongtrinh','Cập nhật thất bại');
                $data['subview'] = 'khachhang/themchuongtrinhapdung';
                $this->load->view("default/default",$data);
            }
        }
    }
}