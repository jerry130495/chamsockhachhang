<?php
/**
 * Created by PhpStorm.
 * User: Tai Tling
 * Date: 3/28/2017
 * Time: 11:29 AM
 */

class C_ChuongTrinh extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("session","form_validation"));
        $this->load->helper("url");
        $this->load->model("Base");
    }

    protected $data;

    public function index(){
        $username = $this->session->userdata('username');
        if(!isset($username)){
            header("location: ".base_url());
        }
        $this->session->set_userdata('objLoaiKH',$this->Base->get_items("LoaiKhachHang","*","","",0,1000));
        $data['chuongtrinh'] = $this->Base->C_get_items("ChuongTrinh,LoaiKhachHang","*", 'LoaiKhachHang.MaLoaiKH = ChuongTrinh.MaLoaiKH', "", "", "", 0, 1000 );
        //print_r($data['select_data']);
        $data['subview'] = 'chuongtrinh/chuongtrinh';
        $this->load->view("default/default",$data);
    }

    public function search(){
        $loaiKH = $this->input->post("loaiKhachHang");
        $tenCT = $this->input->post("tenCT");

        if(isset($loaiKH) && $loaiKH !== 'all'){
            $where = array(
                "ChuongTrinh.MaLoaiKH" => $loaiKH,
                'LoaiKhachHang.MaLoaiKH' => 'ChuongTrinh.MaLoaiKH'
            );
        }

        $where = 'LoaiKhachHang.MaLoaiKH = ChuongTrinh.MaLoaiKH';

        $like = array(
            "TenCT" => $tenCT
        );
        //echo  $this->Base->C_get_items("ChuongTrinh,LoaiKhachHang","*", $where, $like, "", "", 0, 1000 );
        $data['chuongtrinh'] = $this->Base->C_get_items("ChuongTrinh,LoaiKhachHang","*", $where, $like, "", "", 0, 1000 );
        //echo $data['chuongtrinh'];
        $data['subview'] = 'chuongtrinh/chuongtrinh';
        $this->load->view("default/default",$data);
    }

    public function view(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaCT" =>  $this->input->get("id")
            );
            $data['mota'] = $this->Base->get_item("ChuongTrinh","MoTa", $where, FALSE );
            $data['subview'] = 'chuongtrinh/xemchitiet';
            $this->load->view("default/default",$data);
        }
    }

    public function insert(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaCT" =>   $this->input->get("id")
            );
            $data['chuongtrinh'] = $this->Base->get_item("ChuongTrinh","*",$where);
        }
        $data['subview'] = 'chuongtrinh/themchuongtrinh';
        $this->load->view("default/default",$data);
    }

    public function submitInsert(){
        if($this->input->post("themchuongtrinh") != null){
            $dataInsert = array(
                "MaCT" => $this->input->post("MaCT"),
                "TenCT" => $this->input->post("TenCT"),
                "MaLoaiKH" => $this->input->post("MaLoaiKH"),
                "MoTa" => $this->input->post("MoTa")
            );

            if($this->Base->insert("ChuongTrinh", $dataInsert, false) != FALSE){
                $this->session->set_flashdata('MessChuongTrinh','Cập nhật thành công');
                $data['subview'] = 'chuongtrinh/themchuongtrinh';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('MessChuongTrinh','Cập nhật thất bại');
                $data['subview'] = 'chuongtrinh/themchuongtrinh';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'chuongtrinh/chuongtrinh';
            $this->load->view("default/default",$data);
        }
    }

    public function update(){
        if($this->input->post("themchuongtrinh") != null){
            $dataInsert = array(
                "MaCT" => $this->input->post("MaCT"),
                "TenCT" => $this->input->post("TenCT"),
                "MaLoaiKH" => $this->input->post("MaLoaiKH"),
                "MoTa" => $this->input->post("MoTa")
            );
            //var_dump($dataInsert);
            $where = array(
                "MaCT" => $this->input->post("MaCT")
            );

            if($this->Base->update("ChuongTrinh", $dataInsert, $where) != FALSE){
                $this->session->set_flashdata('MessChuongTrinh','Cập nhật thành công');
                $data['subview'] = 'chuongtrinh/themchuongtrinh';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('MessChuongTrinh','Cập nhật thất bại');
                $data['subview'] = 'chuongtrinh/themchuongtrinh';
                $this->load->view("default/default",$data);
            }
        }
        else{
            $data['subview'] = 'chuongtrinh/chuongtrinh';
            $this->load->view("default/default",$data);
        }
    }

    public function delete(){
        if($this->input->get("id") != '' && $this->input->get("id") != null){
            $where = array(
                "MaCT" =>   $this->input->get("id")
            );

            if($this->Base->delete("ChuongTrinh",$where)>0){
                $this->session->set_flashdata('MessChuongTrinh','Cập nhật thành công');
                $data['subview'] = 'chuongtrinh/chuongtrinh';
                $this->load->view("default/default",$data);
            }
            else{
                $this->session->set_flashdata('MessChuongTrinh','Cập nhật thất bại');
                $data['subview'] = 'chuongtrinh/chuongtrinh';
                $this->load->view("default/default",$data);
            }
        }
    }
}