<?php
/**
 * Created by PhpStorm.
 * User: Tai Tling
 * Date: 3/28/2017
 * Time: 11:29 AM
 */

    class C_DangNhap extends CI_Controller {
        public function __construct()
        {
            parent::__construct();
            $this->load->library(array("session","form_validation"));
            $this->load->helper("url");
        }

        protected $data;

        public function index(){
            if($this->session->userdata('username') != null){
                $data['username'] = $this->session->userdata['username'];
            }
            $data['subview'] = 'default/preview';
            $this->load->view("default/default",$data);
        }

        public function login(){
            $data['subview'] = 'login/login';
            $this->load->view("default/default",$data);
        }

        public function submitLogin(){
            $this->load->model("Base");
            $username = $this->input->post("username");
            $password = $this->input->post("password");
            $where = array(
                'MaNV' => $username,
                'MatKhau' => $password
            );
            if($this->input->post("login") != null){
                $objNhanVien = $this->Base->get_item("NhanVien", "HoTenNV",$where, FALSE);
                if($objNhanVien != null){
                    $this->session->set_userdata("username",$objNhanVien->HoTenNV);
                    $this->session->set_userdata("userid", $username);
                    $data['subview'] = 'default/preview';
                    $this->load->view("default/default",$data);
                }
                else{
                    $this->session->set_flashdata("errLogin","Đăng nhập thất bại, vui lòng kiểm tra lại tài khoản hoặc mật khẩu");
                    $data['subview'] = 'login/login';
                    $this->load->view("default/default",$data);
                }
            }
            else{
                $data['subview'] = 'default/preview';
                $this->load->view("default/default",$data);
                $this->session->set_flashdata("errLogin","Đăng nhập thất bại, vui lòng kiểm tra lại tài khoản hoặc mật khẩu");
            }
        }

        public function logout(){
            session_destroy();
            $data['subview'] = 'default/preview';
            $this->load->view("default/default",$data);
        }
    }