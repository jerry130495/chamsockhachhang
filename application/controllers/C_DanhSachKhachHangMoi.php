<?php
/**
 * Created by PhpStorm.
 * User: Tai Lining
 * Date: 3/28/2017
 * Time: 11:29 AM
 */

class C_DanhSachKhachHangMoi extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("session","form_validation"));
        $this->load->helper("url");
        $this->load->model("Base");
    }

    protected $data;

    public function index(){
        $username = $this->session->userdata('username');
        if(!isset($username)){
            header("location: ".base_url());
        }
        $where = "KhachHang.MaKH = BanHoTro.MaKH";
        $data['khachhangmoi'] = $this->Base->get_items("KhachHang, BanHoTro", $select = "*", $where,"",0,30);
        $data['subview'] = 'baocao/danhsachkhachhangmoi';
        $this->load->view("default/default",$data);
    }
}