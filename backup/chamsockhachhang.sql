-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2017 at 03:43 PM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Table structure for table `bandanhgia`
--

CREATE TABLE IF NOT EXISTS `bandanhgia` (
  `MaDanhGia` varchar(10) NOT NULL,
  `MaKH` varchar(10) NOT NULL,
  `YKienDanhGia` varchar(1000) NOT NULL,
  PRIMARY KEY (`MaDanhGia`),
  KEY `FK_BanDanhGia_KhachHang` (`MaKH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `BanHoTro`
--

CREATE TABLE IF NOT EXISTS `BanHoTro` (
  `MaHoTro` varchar(10) NOT NULL,
  `MaKH` varchar(10) NOT NULL,
  `NoiDungYeuCau` varchar(1000) NOT NULL,
  `NgayYeuCau` datetime NOT NULL,
  `MaDichVu` varchar(10) NOT NULL,
  PRIMARY KEY (`MaHoTro`),
  KEY `FK_BanHoTro_KhachHang` (`MaKH`),
  KEY `FK_BanHoTro_DichVu` (`MaDichVu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `BanHoTro`
--

INSERT INTO `BanHoTro` (`MaHoTro`, `MaKH`, `NoiDungYeuCau`, `NgayYeuCau`, `MaDichVu`) VALUES
('HT001', 'KH001', '<p>Đổi lại điện thoại Iphone 7 red bị lỗi 1 321</p>', '2017-03-28 00:00:00', 'DV002');

-- --------------------------------------------------------

--
-- Table structure for table `BanPhanHoi`
--

CREATE TABLE IF NOT EXISTS `BanPhanHoi` (
  `MaPH` varchar(10) NOT NULL,
  `MaKH` varchar(10) NOT NULL,
  `MaNV` varchar(10) NOT NULL,
  `MaHoTro` varchar(10) NOT NULL,
  `NoiDungPH` varchar(50) NOT NULL,
  `NgayPH` datetime NOT NULL,
  PRIMARY KEY (`MaPH`),
  KEY `FK_BanPhanHoi_KhachHang` (`MaKH`),
  KEY `FK_BanPhanHoi_NhanVien` (`MaNV`),
  KEY `FK_BanPhanHoi_BanHoTro` (`MaHoTro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `BanPhanHoi`
--

INSERT INTO `BanPhanHoi` (`MaPH`, `MaKH`, `MaNV`, `MaHoTro`, `NoiDungPH`, `NgayPH`) VALUES
('PH001', 'KH001', 'admin', 'HT001', 'Chúng tôi sẽ đổi lại cho bạn nếu sản phẩm vẫn còn ', '2017-03-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `BanQuanLy`
--

CREATE TABLE IF NOT EXISTS `BanQuanLy` (
  `MaBQL` varchar(10) NOT NULL,
  `TenBQL` varchar(128) NOT NULL,
  `SDT` int(11) DEFAULT NULL,
  `ChucVu` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`MaBQL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ChiTietChuongTrinh`
--

CREATE TABLE IF NOT EXISTS `ChiTietChuongTrinh` (
  `MaCTCT` varchar(10) NOT NULL,
  `MaCT` varchar(10) NOT NULL,
  `MaKH` varchar(10) NOT NULL,
  `MaNV` varchar(10) NOT NULL,
  `Diple` varchar(50) NOT NULL,
  `Ngay` datetime DEFAULT NULL,
  `HinhThuc` varchar(50) DEFAULT NULL,
  `GiaiTri` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MaCTCT`),
  KEY `FK_ChiTietChuongTrinh_ChuongTrinh` (`MaCT`),
  KEY `FK_ChiTietChuongTrinh_KhachHang` (`MaKH`),
  KEY `FK_ChiTietChuongTrinh_NhanVien` (`MaNV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ChuongTrinh`
--

CREATE TABLE IF NOT EXISTS `ChuongTrinh` (
  `MaCT` varchar(10) NOT NULL,
  `TenCT` varchar(50) NOT NULL,
  `MaLoaiKH` varchar(10) NOT NULL,
  `MoTa` varchar(2000) NOT NULL,
  PRIMARY KEY (`MaCT`),
  KEY `FK_ChuongTrinh_LoaiKH` (`MaLoaiKH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ChuongTrinh`
--

INSERT INTO `ChuongTrinh` (`MaCT`, `TenCT`, `MaLoaiKH`, `MoTa`) VALUES
('CT001', 'Hạnh phúc đầu xuân', '1', 'What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). Where does it come from? Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, mak');

-- --------------------------------------------------------

--
-- Table structure for table `DichVu`
--

CREATE TABLE IF NOT EXISTS `DichVu` (
  `MaDichVu` varchar(10) NOT NULL,
  `TenDichVu` varchar(128) NOT NULL,
  PRIMARY KEY (`MaDichVu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DichVu`
--

INSERT INTO `DichVu` (`MaDichVu`, `TenDichVu`) VALUES
('DV001', 'Giảm giá'),
('DV002', 'Trả góp'),
('DV003', 'Đổi trả');

-- --------------------------------------------------------

--
-- Table structure for table `KhachHang`
--

CREATE TABLE IF NOT EXISTS `KhachHang` (
  `MaKH` varchar(10) NOT NULL,
  `TenKH` varchar(128) NOT NULL,
  `DiaChi` varchar(1000) DEFAULT NULL,
  `SDT` int(11) DEFAULT NULL,
  `Email` varchar(128) DEFAULT NULL,
  `NganhNghe` varchar(128) DEFAULT NULL,
  `ChucVu` varchar(128) DEFAULT NULL,
  `MaLoaiKH` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`MaKH`),
  KEY `FK_MaKH_MaLoaiKH` (`MaLoaiKH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `KhachHang`
--

INSERT INTO `KhachHang` (`MaKH`, `TenKH`, `DiaChi`, `SDT`, `Email`, `NganhNghe`, `ChucVu`, `MaLoaiKH`) VALUES
('KH001', 'Tùng', 'Hà Nội', 1683111111, 'tung@gmail.com', 'Lập trình viên Freelacer', 'Chủ tịch', '1');

-- --------------------------------------------------------

--
-- Table structure for table `LoaiKhachHang`
--

CREATE TABLE IF NOT EXISTS `LoaiKhachHang` (
  `MaLoaiKH` varchar(10) NOT NULL,
  `TenLoaiKH` varchar(128) NOT NULL,
  PRIMARY KEY (`MaLoaiKH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `LoaiKhachHang`
--

INSERT INTO `LoaiKhachHang` (`MaLoaiKH`, `TenLoaiKH`) VALUES
('1', 'Vip'),
('2', 'Tiềm năng'),
('3', 'Thường');

-- --------------------------------------------------------

--
-- Table structure for table `NhanVien`
--

CREATE TABLE IF NOT EXISTS `NhanVien` (
  `MaNV` varchar(10) NOT NULL,
  `MatKhau` varchar(64) NOT NULL,
  `HoTenNV` varchar(50) NOT NULL,
  `SDT` int(11) DEFAULT NULL,
  `DiaChi` varchar(1000) DEFAULT NULL,
  `ChucVu` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`MaNV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `NhanVien`
--

INSERT INTO `NhanVien` (`MaNV`, `MatKhau`, `HoTenNV`, `SDT`, `DiaChi`, `ChucVu`) VALUES
('admin', 'test', 'Administrator', 1111111111, 'Hà Nội', 'admin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bandanhgia`
--
ALTER TABLE `bandanhgia`
  ADD CONSTRAINT `FK_BanDanhGia_KhachHang` FOREIGN KEY (`MaKH`) REFERENCES `KhachHang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `BanHoTro`
--
ALTER TABLE `BanHoTro`
  ADD CONSTRAINT `FK_BanHoTro_DichVu` FOREIGN KEY (`MaDichVu`) REFERENCES `DichVu` (`MaDichVu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_BanHoTro_KhachHang` FOREIGN KEY (`MaKH`) REFERENCES `KhachHang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `BanPhanHoi`
--
ALTER TABLE `BanPhanHoi`
  ADD CONSTRAINT `FK_BanPhanHoi_BanHoTro` FOREIGN KEY (`MaHoTro`) REFERENCES `BanHoTro` (`MaHoTro`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_BanPhanHoi_KhachHang` FOREIGN KEY (`MaKH`) REFERENCES `KhachHang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_BanPhanHoi_NhanVien` FOREIGN KEY (`MaNV`) REFERENCES `NhanVien` (`MaNV`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ChiTietChuongTrinh`
--
ALTER TABLE `ChiTietChuongTrinh`
  ADD CONSTRAINT `FK_ChiTietChuongTrinh_NhanVien` FOREIGN KEY (`MaNV`) REFERENCES `NhanVien` (`MaNV`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ChiTietChuongTrinh_ChuongTrinh` FOREIGN KEY (`MaCT`) REFERENCES `ChuongTrinh` (`MaCT`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ChiTietChuongTrinh_KhachHang` FOREIGN KEY (`MaKH`) REFERENCES `KhachHang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ChuongTrinh`
--
ALTER TABLE `ChuongTrinh`
  ADD CONSTRAINT `FK_ChuongTrinh_LoaiKH` FOREIGN KEY (`MaLoaiKH`) REFERENCES `LoaiKhachHang` (`MaLoaiKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `KhachHang`
--
ALTER TABLE `KhachHang`
  ADD CONSTRAINT `FK_MaKH_MaLoaiKH` FOREIGN KEY (`MaLoaiKH`) REFERENCES `LoaiKhachHang` (`MaLoaiKH`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
